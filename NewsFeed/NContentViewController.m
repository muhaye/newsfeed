//
//  NContentViewController.m
//  NewsFeed
//
//  Created by Ngendo Muhayimana on 2012-11-29.
//  Copyright (c) 2012 MUH Mobile Inc. All rights reserved.
//

#import "NContentViewController.h"

@interface NContentViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation NContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    NSLog(@"contentURL : %@",self.contentURL);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:self.contentURL];
    
    [self.webView loadRequest:request];
    
}

@end
