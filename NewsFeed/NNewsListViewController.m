//
//  NNewsListViewController.m
//  NewsFeed
//
//  Created by Ngendo Muhayimana on 2012-11-23.
//  Copyright (c) 2012 MUH Mobile Inc. All rights reserved.
//

#import "NNewsListViewController.h"
#import "NNewsCell.h"
#import "NContentViewController.h"

@interface NNewsListViewController ()

@property (strong, nonatomic) IBOutlet UICollectionView *nCollectionView;
@property (nonatomic,strong) NSMutableArray * newsList;
@property (assign) NSInteger depth;
@property (strong, nonatomic) NSString *currentElement;
@property (strong, nonatomic) NSString *currentName;
@property (strong, nonatomic) NSMutableDictionary *item;

@end

@implementation NNewsListViewController

@synthesize feedURL;
@synthesize newsList;
@synthesize depth;
@synthesize currentElement;
@synthesize currentName;
@synthesize item;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.newsList = [[NSMutableArray alloc] init];

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self refresh];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma UICollectionViewDataSource Protocol Reference

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.newsList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NNewsCell*cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NNewsCell" forIndexPath:indexPath];
    
    NSDictionary *currentItem = [self.newsList objectAtIndex:indexPath.row];
    
    cell.title.text     = [currentItem objectForKey:@"title"];
    cell.summary.text     = [currentItem objectForKey:@"summary"];
    cell.date.text     = [currentItem objectForKey:@"published"];
    
    
    
    return cell;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NContentViewController *nContentViewController = segue.destinationViewController;
    
    
    NSInteger index = ((NSIndexPath *)[self.collectionView.indexPathsForSelectedItems lastObject]).row;
    
    NSDictionary *itemSelected = [self.newsList objectAtIndex:index];
    
    NSURL *webLink = [NSURL URLWithString:[itemSelected objectForKey:@"link"]];
    
    nContentViewController.contentURL   =  webLink;
    
    
}


#pragma mark -
#pragma mark NSXMLParserDelegate methods

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
    NSLog(@"Document started", nil);
    depth = 0;
    currentElement = nil;
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    NSLog(@"Error: %@", [parseError localizedDescription]);
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict
{
    
    currentElement = [elementName copy];
    
    if ([currentElement isEqualToString:@"link"] &&  depth==1 ) {
        NSLog(@"attributeDict : %@",attributeDict );
        [self.item setObject:[attributeDict objectForKey:@"href"] forKey:@"link"];
    }
    
    if ([currentElement isEqualToString:@"entry"]){
        ++depth;
        self.item      = [[NSMutableDictionary alloc] init];
        
        //[self showCurrentDepth];
    }else if ((  [currentElement isEqualToString:@"summary"]
               || [currentElement isEqualToString:@"title"]
               || [currentElement isEqualToString:@"published"] ) && depth==1){
        
        
        self.currentName = [[NSMutableString alloc] init];
        
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
{
    
    if ([elementName isEqualToString:@"entry"]){
        --depth;
        NSLog(@"entry :%@",self.item);
        [self.newsList addObject:[self.item copy]];
        
    }else     if ((  [currentElement isEqualToString:@"summary"]
                   || [currentElement isEqualToString:@"title"]
                   || [currentElement isEqualToString:@"published"] ) && depth==1){
        
        [self.item setObject:self.currentName forKey:currentElement];
        
    }
    
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    
    NSLog(@"self.currentElement  : %@",self.currentElement );
    
    if ((  [currentElement isEqualToString:@"summary"]
         || [currentElement isEqualToString:@"title"]
         || [currentElement isEqualToString:@"published"] ) && depth==1){
        
        
        self.currentName = [@"" stringByAppendingFormat:@"%@%@",self.currentName, string];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser{
    
    NSLog(@"Document finished%@", self.newsList);
    
    [self.nCollectionView reloadData];
}


#pragma -
#pragma request

-(void)refresh{

    NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:self.feedURL];
    
    parser.delegate = self;
    
    [parser parse];
    
}


@end
