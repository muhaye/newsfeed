//
//  main.m
//  NewsFeed
//
//  Created by Ngendo Muhayimana on 2012-11-23.
//  Copyright (c) 2012 MUH Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NAppDelegate class]));
    }
}
