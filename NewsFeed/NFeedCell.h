//
//  NFeedCell.h
//  NewsFeed
//
//  Created by Ngendo Muhayimana on 2012-11-23.
//  Copyright (c) 2012 MUH Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NFeedCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
