//
//  NContentViewController.h
//  NewsFeed
//
//  Created by Ngendo Muhayimana on 2012-11-29.
//  Copyright (c) 2012 MUH Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NContentViewController : UIViewController

@property (nonatomic,strong) NSURL *contentURL;


@end
