//
//  NFeedsCollectionViewController.m
//  NewsFeed
//
//  Created by Ngendo Muhayimana on 2012-11-23.
//  Copyright (c) 2012 MUH Mobile Inc. All rights reserved.
//

#import "NFeedsCollectionViewController.h"
#import "NFeedCell.h"
#import <QuartzCore/QuartzCore.h>
#import "NNewsListViewController.h"

@interface NFeedsCollectionViewController ()
@property (nonatomic,strong)NSArray*feeds;
@property (strong, nonatomic) IBOutlet UICollectionView *nCollectionView;



@end

@implementation NFeedsCollectionViewController

@synthesize feeds;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSString *path = [[NSBundle mainBundle] pathForResource:@"feeds" ofType:@"plist"];
    self.feeds      = [[NSArray alloc] initWithContentsOfFile:path];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    NNewsListViewController *nNewsListViewController = segue.destinationViewController;
    
    NSInteger index = ((NSIndexPath*)[[self.collectionView indexPathsForSelectedItems] lastObject]).row;
    
    nNewsListViewController.feedURL = [NSURL URLWithString:[[self.feeds objectAtIndex:index] objectForKey:@"url"] ];

}


#pragma UICollectionViewDataSource Protocol Reference

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.feeds.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NFeedCell*cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NFeedCell" forIndexPath:indexPath];
    
    NSDictionary *item = [self.feeds objectAtIndex:indexPath.row];
    
    cell.imageView.image = [UIImage imageNamed:[item objectForKey:@"img"]];
    cell.title.text     =  [item objectForKey:@"name"];
    
    
    cell.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.layer.borderWidth  = 2.0f;
    cell.layer.cornerRadius = 2.0;
    
    
    return cell;
}



@end
